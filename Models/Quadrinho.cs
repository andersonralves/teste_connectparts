﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TesteConnectParts.Models
{
    public class Quadrinho
    {
        public int Id { get; set; }

        public int IdPersonagem { get; set; }
        public string Title { get; set; }
        public string Descricao { get; set; }
        public string UrlImagem { get; set; }
        public string UrlWiki { get; set; }
    }
}