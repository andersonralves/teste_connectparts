﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;
using TesteConnectParts.Models;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace TesteConnectParts.Controllers
{
    public class HomeController : Controller
    {
        private string domain; 
        private string publicKey;
        private string privateKey;
        private string ts;
        private string hash;
        private HttpClient client;
        private Personagem personagem;
        private string parametrosObrigatorios;

        public HomeController()
        {
            this.domain = ConfigurationManager.AppSettings["MarvelDomain"];
            this.publicKey = ConfigurationManager.AppSettings["MarvelPublicKey"];
            this.privateKey = ConfigurationManager.AppSettings["MarvelPrivateKey"];
            this.ts = DateTime.Now.Ticks.ToString();
            this.hash = GerarHash(this.publicKey, this.privateKey, ts);

            this.parametrosObrigatorios = $"ts={ts}&apikey={publicKey}&hash={hash}";

        }

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Personagens()
        {

            List<Personagem> personagens = new List<Personagem>();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                HttpResponseMessage response = client.GetAsync(domain + $"characters?" + this.parametrosObrigatorios).Result;
                response.EnsureSuccessStatusCode();

                string conteudo = response.Content.ReadAsStringAsync().Result;
                dynamic resultado = JsonConvert.DeserializeObject(conteudo);

                foreach (var item in resultado.data.results)
                {
                    Personagem personagem = new Personagem();

                    personagem.Id = item.id;
                    personagem.Nome = item.name;
                    personagem.Descricao = item.description;
                    personagem.UrlImagem = item.thumbnail.path + "." + item.thumbnail.extension;
                    personagem.UrlWiki = item.urls[1].url;

                    personagens.Add(personagem);
                }

            }

            ViewBag.personagens = personagens;

            return View();
        }

        public ActionResult Quadrinhos(string codigoPersonagem)
        {
            List<Quadrinho> quadrinhos = new List<Quadrinho>();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                HttpResponseMessage response = client.GetAsync(this.domain + $"characters/{codigoPersonagem}/comics?" + this.parametrosObrigatorios).Result;
                response.EnsureSuccessStatusCode();

                string conteudo = response.Content.ReadAsStringAsync().Result;
                dynamic resultado = JsonConvert.DeserializeObject(conteudo);

                foreach (var item in resultado.data.results)
                {
                    Quadrinho quadrinho = new Quadrinho();

                    quadrinho.Id = item.id;
                    quadrinho.IdPersonagem = Convert.ToInt32(codigoPersonagem);
                    quadrinho.Title = item.title;
                    quadrinho.Descricao = item.description;
                    quadrinho.UrlImagem = item.thumbnail.path + "." + item.thumbnail.extension;
                    quadrinho.UrlWiki = item.urls[0].url;

                    quadrinhos.Add(quadrinho);
                }

            }

            ViewBag.quadrinhos = quadrinhos;

            return View();
        }

        /// <summary>
        /// Gera o hash para envio na requisição
        /// </summary>
        /// <param name="publicKey"></param>
        /// <param name="privateKey"></param>
        /// <param name="ts"></param>
        /// <returns></returns>
        private string GerarHash(string publicKey, string privateKey, string ts)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(ts + privateKey + publicKey);
            var gerador = MD5.Create();
            byte[] bytesHash = gerador.ComputeHash(bytes);

            return BitConverter.ToString(bytesHash).ToLower().Replace("-", string.Empty);
        }
    }
}